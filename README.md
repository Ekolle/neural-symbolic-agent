

Contains the source code of 4 scenarios of a Neural Symbolic Agent which recognize sequence of handwriten digits from the MNIST dataset and deduce if the first digit between any two sequence of digit, is less than the second digit.

## Prerequisites
   
   -Python >= 3.6.
   
   -Kares >=2.3 (https://keras.io/).
   
   -Numpy (https://numpy.org/).
   
   -matplotlib (https://matplotlib.org/).

## Folder structure

   "root" is the top-level folder. It contains the folders of all the four scenarios
   
   "Sceneario-1_T1-E1" includes the source code of scenario 1 ie train T1 and test E1, which include code to generate their performance evaluation graphs.
   
   "Sceneario-2_T1-E2" includes the source code of scenario 2 ie train T1 and test E2, which include code to generate their performance evaluation graphs.
   
   "Sceneario-3_T2-E1" includes the source code of scenario 3 ie train T2 and test E1, which include code to generate their performance evaluation graphs.
   
   "Sceneario-4_T2-E2" includes the source code of scenario 4 ie train T2 and test E2, which include code to generate their performance evaluation graphs.

# Naming convention
  
  T1: Use the training set, and create random pairs of digits such that each pair contains either two odd or two even digits.
  
  T2: Use the training set, and create random pairs of digits such that each pair contains exactly one odd and one even digit.

  E1: Use the testing set, and create random pairs of digits such that each pair contains either two odd or two even digits.
  
  E2: Use the testing set, and create random pairs of digits such that each pair contains exactly one odd and one even digit.


## Assumptions

   The Symbolic module is base on a set of fixed declarative (or deductive) statements which form its theory. 
   
   Learning is done only with the Neural module to ameliorate its predictions (or induced values).   

## Training and Testing
   
   Scenario-1: Train on T1, test on E1.
   
   Scenario-2: Train on T1, test on E2.
   
   Scenario-3: Train on T2, test on E1.
   
   Scenario-4: Train on T2, test on E2.

   For each scenario, the Neural Symbolic model is train with the corresponding training dataset and at same time validated with the testing dataset.
   
   After training, the accuracy of induction and deduction is use to define the performance of the Neural Symbolic agent on the test dataset.
   