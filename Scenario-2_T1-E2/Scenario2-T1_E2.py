# Scenario 2 : train on T1 dataset and Test on E2 dataset
from numpy import mean
from matplotlib import pyplot
from keras.datasets import mnist
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.optimizers import Adam
import itertools 
from itertools import chain
from matplotlib import pyplot
import numpy as np
 
# i-Dataset preparation
def data_preparation():
  # Load data
  (trainX, trainY), (testX, testY) = mnist.load_data(path='mnist.npz')

  # Training Data reorganization
  trainX_evenList=[]  #list to store even number
  trainY_evenList=[]
  trainX_oddList=[]   #list to store odd number
  trainY_oddList=[]
  for j,k in zip(trainY,trainX):
    if j%2==0:
        trainY_evenList.append(j)
        trainX_evenList.append(k)
    else:
        trainY_oddList.append(j)
        trainX_oddList.append(k)
  #T1: random pairs of training data digits, each pair contains either two odd or two even digits.
  T1X=[]
  T1Y=[]
  T1Y=list(itertools.chain.from_iterable(zip(*zip(*[trainY_evenList[i:i+2] for i in range(0, len(trainY_evenList), 2)]), *zip(*[trainY_oddList[i:i+2] for i in range(0, len(trainY_oddList), 2)]))))
  T1X=list(itertools.chain.from_iterable(zip(*zip(*[trainX_evenList[i:i+2] for i in range(0, len(trainX_evenList), 2)]), *zip(*[trainX_oddList[i:i+2] for i in range(0, len(trainX_oddList), 2)]))))

  # Test Data reorganization
  testX_evenList=[]  #list to store even number
  testY_evenList=[]
  testX_oddList=[] #list to store odd number
  testY_oddList=[]
  for j,k in zip(testY,testX):
    if j%2==0:
        testY_evenList.append(j)
        testX_evenList.append(k)
    else:
        testY_oddList.append(j)
        testX_oddList.append(k)
  #E2: random pairs of training data digits, each pair contains either one odd or one even digits.
  E1X=[]
  E1Y=[]
  E1Y=list(itertools.chain.from_iterable(zip(*zip(*[testY_evenList[i:i+1] for i in range(0, len(testY_evenList), 1)]), *zip(*[testY_oddList[i:i+1] for i in range(0, len(testY_oddList), 1)]))))
  E1X=list(itertools.chain.from_iterable(zip(*zip(*[testX_evenList[i:i+1] for i in range(0, len(testX_evenList), 1)]), *zip(*[testX_oddList[i:i+1] for i in range(0, len(testX_oddList), 1)]))))

  # Source Dataset reshape
  T1X = np.array(T1X)
  E1X = np.array(E1X)
  T1X = T1X.reshape((T1X.shape[0], 28, 28, 1))
  E1X = E1X.reshape((E1X.shape[0], 28, 28, 1))

  #Encode target label (categorical encoding)
  T1Y_enc = to_categorical(T1Y)
  E1Y_enc = to_categorical(E1Y)
 
  return T1X, T1Y_enc, E1X, E1Y_enc, E1Y
 
# scale pixels
def prep_pixels(train, test):
	# convert from integers to floats
	train_norm = train.astype('float32')
	test_norm = test.astype('float32')
	# normalize to range 0-1
	train_norm = train_norm / 255.0
	test_norm = test_norm / 255.0
	# return normalized images
	return train_norm, test_norm
 
# ii-Define neural module
def neural_module():
	model = Sequential()
	model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', input_shape=(28, 28, 1)))
	model.add(MaxPooling2D((2, 2)))
	model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform'))
	model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform'))
	model.add(MaxPooling2D((2, 2)))
	model.add(Flatten())
	model.add(Dense(100, activation='relu', kernel_initializer='he_uniform'))
	model.add(Dense(10, activation='softmax'))
	# compile model
	opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
	model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy']) # we can likewise use adam optimizer
	return model

#iii-Define symbolic module
def symbolic_module(induced_vals, testY_nonenc):
  deduced_val=[]
  test_val=[]
  for i in range(0,len(induced_vals)):
    j=i+1
    if j<len(induced_vals):
      if induced_vals[j]>induced_vals[i]:
        deduced_val.append(1) #true
      else:
        deduced_val.append(0) #false
    else: 
      break
  j=0
  for i in range(len(testY_nonenc)):
    j=i+1
    if j<len(testY_nonenc):
      if testY_nonenc[j]>testY_nonenc[i]:
        test_val.append(1) #true
      else: 
        test_val.append(0) #false
    else: 
      break

  return deduced_val,test_val

# iv-Summary of Neural module learning performance
def neural_summarization(hist):
  # summarize history for loss
  pyplot.plot(hist.history['loss'])
  pyplot.plot(hist.history['val_loss'])
  pyplot.title('Model for T1, E2 : Neural Module Loss (Cross-entropy) evolution per epoch during training ')
  pyplot.ylabel('loss')
  pyplot.xlabel('epoch')
  pyplot.legend(['train', 'test'], loc='upper left')
  pyplot.show()
  # summarize history for accuracy
  pyplot.plot(hist.history['accuracy'])
  pyplot.plot(hist.history['val_accuracy'])
  pyplot.title('Model for T1, E2 : Neural module Accuracy evolution per epoch during training')
  pyplot.ylabel('accuracy')
  pyplot.xlabel('epoch')
  pyplot.legend(['train', 'test'], loc='upper left')
  pyplot.show()

# v-Build Neural Symbolic Agent
def Neural_Symbolic_Agent():
  # load data
  trainX, trainY, testX, testY, testY_nonenc = data_preparation()
  # load source domain pixel data
  trainX, testX = prep_pixels(trainX, testX)

  # 1- Inductive reasoning: Neural module
  # call neural module  
  cnn = neural_module()
  #train neural module with training data and validate with test data after each epoch
  hist = cnn.fit(trainX, trainY, epochs=10, batch_size=32, validation_data=(testX, testY), verbose=0)
  # Summary of Neural module learning performance (loss and RoC curve) on the 12 epochs
  neural_summarization(hist)
  # Neural module prediction on test data
  induced_vals=cnn.predict_classes(testX)
  # prediction performance of neural module on test data
  _, induction_acc = cnn.evaluate(testX, testY, verbose=0)
  print('Accuracy of Neural Module on test data after training = %.2f' % (induction_acc * 100.0))
 
  # 2- Deductive reasoning: Symbolic Module
  #call symbolic module and evalute performance
  deduced_vals, test_vals=symbolic_module(induced_vals, testY_nonenc)
  # deduction performance of Symbolic module on induced results from Neural module
  deduction_acc=(sum(1 for x,y in zip(deduced_vals,test_vals) if x == y) / len(deduced_vals))
  print('Accuracy of Symbolic Module on test data = %.2f' % (deduction_acc * 100.0))
  print('Accuracy of Neural Symbolic Agent = Accurary of Symbolic Module on test data = %.2f' % (deduction_acc * 100.0))

# vi-Execute Neural-Symbolic Agent
Neural_Symbolic_Agent()